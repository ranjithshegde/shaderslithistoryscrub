# History scrubber

This project uses texture arrays as stroage for 300 consequent frames from a
hardware camera. The texture can be sampled on call and drawn to pixels. The
mouse movements point to the index inside the array. Essentially the mouse
functions as a scrubber for on screen pixel to be replaced by one from history

The texture are faked to be an array by storing them using a z-buffer index.
Z index is generally used to indicate nearness to the eye or the importance of
the frame level. In this isntance it simply serves as the index of order for a
quasi array

```cpp
    // Allocate and store texture with z index to fake an array
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D_ARRAY, texture);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, GL_RGB8, width, height, layerCount);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

```

The texture data from each frame is stored in a deque that communucates with the
array defined above

```cpp
    // Store the new frames in a deque structure. New ones come in the front, old ones pop out
    if (camera.isFrameNew()) {
        frames.push_front(camera.getPixels());
    }
    if (frames.size() > N) {
        frames.pop_back();
    }

```

All the sampling, scrubbing and index calculation is handled direction and
parallelly on the graphic card inside the fragment shader

```cpp
uniform sampler2DArray texArray;
uniform vec2 resolution;
uniform vec2 mousePos;
uniform int fSize;
in vec2 texCoord;
out vec4 outputColor;

void main()
{

    // Normalize the texture coordinate against the desired resolution
    vec2 tc = texCoord / resolution;
    // Calculate the magnitude of the vector formed between the current texture
    // co-ordinate and the mouse position
    float dist = distance(mousePos.xy, texCoord.xy);
    // Use larger distance to avoid performance penalty over redundant data
    float f = dist / 8;

    // The desired index is calculated by truncating the float to an int and
    // incrementing it.
    int i0 = int(f);
    int i1 = i0 + 1;

    float interopAmount = i1 - f;

    i0 = clamp(i0, 0, fSize);
    i1 = clamp(i1, 0, fSize);

    // The index is used to locate the new texture to be sampled from the array
    vec3 col0 = texture(texArray, vec3(tc, i0)).rgb;
    vec3 col1 = texture(texArray, vec3(tc, i1)).rgb;

    // The output pixel is the direct result of this sampling
    outputColor = mix(vec4(col0, 1.0), vec4(col1, 1.0), interopAmount);
    /* outputColor = vec4(newi0, newi1, 0.0, 1.0); */
}
```

In future projects, instead of using a camera, the results of the previous
animations or any variety of graphics processing can be first stored in a
framebuffer, and each framebuffer can be stored in the deque the same way camera
frames are. This will allow the user to retain history of as many previous
frames (events) as hardware memory allows for. This however, is just a guide to
illustrate its possibilites, using camera for simplicity of receiving a stream.
