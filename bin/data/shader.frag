#version 460

uniform sampler2DArray texArray;
uniform vec2 resolution;
/* uniform vec2 mouseNorm; */
uniform vec2 mousePos;
uniform int fSize;
in vec2 texCoord;
out vec4 outputColor;

void main()
{

    // Normalize the texture coordinate against the desired resolution
    vec2 tc = texCoord / resolution;
    // Calculate the magnitude of the vector formed between the current texture
    // co-ordinate and the mouse position
    float dist = distance(mousePos.xy, texCoord.xy);
    // Use larger distance to avoid performance penalty over redundant data
    float f = dist / 8;

    // The desired index is calculated by truncating the float to an int and
    // incrementing it.
    int i0 = int(f);
    int i1 = i0 + 1;

    float interopAmount = i1 - f;

    i0 = clamp(i0, 0, fSize);
    i1 = clamp(i1, 0, fSize);

    /* float newi0 = i0 / 75.0; */
    /* float newi1 = i1 / 75.0; */

    // The index is used to locate the new texture to be sampled from the array
    vec3 col0 = texture(texArray, vec3(tc, i0)).rgb;
    vec3 col1 = texture(texArray, vec3(tc, i1)).rgb;

    // The output pixel is the direct result of this sampling
    outputColor = mix(vec4(col0, 1.0), vec4(col1, 1.0), interopAmount);
    /* outputColor = vec4(newi0, newi1, 0.0, 1.0); */
}
