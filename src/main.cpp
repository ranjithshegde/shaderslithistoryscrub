#include "ofApp.h"
#include "ofAppGLFWWindow.h"
#include "ofAppRunner.h"
#include "ofMain.h"

//========================================================================
int main()
{

    ofGLFWWindowSettings settings;
    settings.setGLVersion(4, 6);
    ofCreateWindow(settings);
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp(new ofApp());
}
