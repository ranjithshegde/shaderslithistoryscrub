#include "ofApp.h"
#include "ofGraphics.h"
#include "ofMath.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    cout << " GL VERSION: " << glGetString(GL_VERSION) << endl;
    sampleRate = 48000;
    bufferSize = 512;

    ofSoundStreamSettings settings;
    settings.setOutListener(ofGetAppPtr());
    settings.sampleRate = sampleRate;
    settings.bufferSize = bufferSize;
    settings.numOutputChannels = 2;
    settings.numBuffers = 2;
    soundStream.setup(settings);

    N = 150;

    width = 1920;
    height = 1080;
    layerCount = N;
    mipLevelCount = 1;

    camera.initGrabber(width, height);
    ofSetLogLevel(OF_LOG_VERBOSE);
    shader.load("shader.vert", "shader.frag");

    // Allocate and store texture with z index to fake an array
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D_ARRAY, texture);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, GL_RGB8, width, height, layerCount);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

//--------------------------------------------------------------
void ofApp::update()
{
    camera.update();
    float mX = ofClamp((mouseX / (float)ofGetWidth()), 0.0, 1.0);
    float mY = ofClamp((mouseY / (float)ofGetHeight()), 0.0, 1.0);
    mousePos = { mX, mY };

    // Store the new frames in a deque structure. New ones come in the front, old ones pop out
    if (camera.isFrameNew()) {
        frames.push_front(camera.getPixels());
    }
    if (frames.size() > N) {
        frames.pop_back();
    }

    glBindTexture(GL_TEXTURE_2D_ARRAY, texture);
    if (!frames.empty()) {
        for (unsigned i = 0; i < frames.size(); i++) {
            glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, width, height, 1, GL_RGB, GL_UNSIGNED_BYTE, frames[i].getData());
        }
    }
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackground(255, 255, 255);

    // glActiveTexture(GL_TEXTURE0 + texture);
    // glClientActiveTexture(GL_TEXTURE0 + texture);
    // glEnable(GL_TEXTURE_2D_ARRAY);
    // glBindTexture(GL_TEXTURE_2D_ARRAY, texture);

    shader.begin();
    shader.setUniform2f("mousePos", abs(mouseX), abs(mouseY));
    shader.setUniform1i("fSize", frames.size());
    shader.setUniformTexture("texArray", GL_TEXTURE_2D_ARRAY, texture, 0);
    shader.setUniform2f("resolution", frames[0].getWidth(), frames[0].getHeight());
    camera.draw(0, 0);
    shader.end();
    // glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

//--------------------------------------------------------------
void ofApp::audioOut(ofSoundBuffer& buffer)
{
    float currentSample = 0;
    for (size_t i = 0; i < buffer.getNumFrames(); i++) {
        buffer[i * buffer.getNumChannels()] = currentSample;
        buffer[i * buffer.getNumChannels() + 1] = currentSample;
    }
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key) { }

//--------------------------------------------------------------
void ofApp::keyReleased(int key) { }

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) { }

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) { }

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) { }

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) { }

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) { }

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) { }

//--------------------------------------------------------------
void ofApp::exit()
{
    soundStream.close();
}
